#include <fstream>
#include <string>
class trie {
	protected:
		int *t;
		int length;
		int nbr;
		std::ofstream *fichier;
	public:
		void Affiche();
		void Affiche(int *,int);
		int getLength();
		int *getTab();
		int getNbr();
		void ecritFichier();
		void ecritFichier(int *,int);
		void setFichier(std::ofstream*);
		virtual std::string getType();
		virtual void tri();
};
class triRapide: public trie {
	public:
		triRapide(int *,int);
		virtual void tri();
		void partition(int *,int,int,int);
		virtual std::string getType();
};
class triFusion: public trie {
	public:
		triFusion(int *,int);
		int *Fusion(int*,int,int*,int);
		int *tri_fusion(int *,int); 
		virtual void tri();
		virtual std::string getType();
};
class triParTas: public trie {
	public:
		triParTas(int *,int);
		void entasse();
		void tamise(int,int);
		void percole();
		virtual void tri();
		virtual std::string getType();
};
