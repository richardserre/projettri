class trie {
	protected:
		int *t;
		int length;
		int nbr;
		
	public:
	
		void Affiche();
		void Affiche(int *,int);
		int getLength();
		int *getTab();
		virtual void tri();
};
