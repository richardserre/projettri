Main: Main.o triParTas.o triFusion.o triRapide.o Client.o trie.o
	g++ -o $@ $^
	
Main.o: Main.cpp Client.h declarationclasses.h 
	g++ -Wall -c $<
	
triParTas.o: triParTas.cpp declarationclasses.h
	g++ -Wall -c $<
	
triFusion.o: triFusion.cpp declarationclasses.h
	g++ -Wall -c $<
	
triRapide.o: triRapide.cpp declarationclasses.h
	g++ -Wall -c $<
	
trie.o: trie.cpp declarationclasses.h
	g++ -Wall -c $<
	
Client.o: Client.cpp Client.h declarationclasses.h
	g++ -Wall -c $<

Main.pdf: Main.cpp declarationclasses.h triParTas.cpp triFusion.cpp triRapide.cpp Client.cpp Client.h Makefile
	a2ps -R -o - $^ | ps2pdf - $@

clean: 
	rm -f *.gch
	rm -f *.o
	rm -f Main
	rm -f Main.pdf
 

