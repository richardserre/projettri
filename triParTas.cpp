#include <iostream>
#include "declarationclasses.h"


using namespace std;

triParTas::triParTas(int *t,int length) {
	
	this->t=t;
	this->length=length;
	this->nbr=0;
}
void triParTas::tri() {
	entasse();
	percole();
	
	*fichier << "iteration -> " << nbr << endl;
}
std::string triParTas::getType() {
	return "tri par tas";	
}
void triParTas::entasse() {
	for (int i=length/2+1;i>=0;i--) {
		tamise(i,length);
		ecritFichier(t,length);
		*fichier << endl;
	}
}
void triParTas::percole() {
	for (int i=length-1;i>0;i--) {
		std::swap(t[0],t[i]);
		tamise(0,i-1);
		ecritFichier(t,length);
		*fichier << endl;
		
	}		
	if (t[0]>t[1]) {
		std::swap(t[0],t[1]);
	}
	//~ ecritFichier(t,length);
	//~ *fichier << endl;
}
void triParTas::tamise(int noeud,int n) {
	int pere=noeud;
	int fils=2*pere+1;
	nbr++;
	while (fils<n) {
		if ((t[fils]<t[fils+1])) {
			fils++;
			if (fils==length)
				fils--;
		}
		if (t[pere]<t[fils]) {
			std::swap(t[pere],t[fils]);
			pere=fils;
			fils=2*pere+1;
		} else {
			break;	
		}
	}
}
