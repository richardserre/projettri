#include <iostream>

#include "declarationclasses.h"


using namespace std;

triRapide::triRapide(int *t,int length) {
	this->t=t;
	this->length=length;
	this->nbr=0;
	
}
void triRapide::tri() {
	partition(t,length,0,length-1);
	*fichier << "iteration -> " << nbr << endl;
}
std::string triRapide::getType() {
	return "tri rapide";	
}
void triRapide::partition(int *t,int t_length,int left,int right) {
	int i=left,j=right-1;
	int pivot=(left+right)/2;
	int val_pivot=t[pivot];
	if (left>=right)
		return;
	std::swap(t[pivot],t[right]);
	while (i<=j) {
		while ((t[i]<val_pivot)&&(i<=right))
			i++;
		while ((t[j]>val_pivot)&&(j>=left))
			j--;
		if (i<=j) {
			std::swap(t[i],t[j]);
			i++;
			j--;
		}	
	}
	nbr++;
	std::swap(t[right],t[i]);
	ecritFichier();

	if (left<j)
		partition (t,length,left,j);
	if (i<right)
		partition (t,length,i+1,right);
}
