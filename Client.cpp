#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>  
#include "Client.h"

using namespace std;

Client::Client(int n) {
	this->length=n;
	this->borne_max=500;

}	
Client::Client() {
	this->length=10;
	this->borne_max=500;
}
void Client::determinePathFichier() {
	char *path=NULL;
    size_t size=200;
    path=getcwd(path,size);
    cout<<"\n current Path"<<path;
	this->fichier=new ofstream("tri.txt");	
}
void Client::determineLength() {
	cout << "combien d entiers dans le tableau a trier par ordre croissant de valeurs?" << endl;
	cin >> this->length;
		
}
void Client::generetab() {
	srand(time(NULL));
	tab=new int[length];
	for (int i=0;i<length;i++)
		tab[i]=rand()%borne_max;
}

void Client::determineTri() {
	cout << "quel type de tri:" << endl;
	cout << "1-> tri rapide" << endl;
	cout << "2-> tri fusion" << endl;
	cout << "3-> tri par tas" << endl;
	int choix=0;
	cin >> choix;
	if (choix==1) {
		tr=new triRapide(tab,length);
	}
	if (choix==2) {
		tr=new triFusion(tab,length);
	}
	if (choix==3) {
		tr=new triParTas(tab,length);
	}
	
}
void Client::ecritFichier() {
	for (int i=0;i<length;i++) {
	*fichier << tab[i] ;
		if (i<(length-1))
			*fichier << "-" ;
	}	
	*fichier << endl;
}
void Client::run () {
	*fichier << tr->getType().c_str() << endl;
	tr->setFichier(fichier);
	clock_t debut,fin;
	debut=clock();
	tr->tri();
	fin=clock();
	float res = (fin - debut) / (float)CLOCKS_PER_SEC;
	*fichier << "temps d execution : " << res << endl; 
}
