#include <fstream>
#include "declarationclasses.h"

class Client {
	private:
		char nomfichier[50]="TriDeNombres.txt";
		std::ofstream *fichier;
		trie *tr;
		int *tab;
		int length;
		int borne_max;
		
	public:
		Client ();
		Client (int n);
		void determinePathFichier(); 
		void generetab();
		void determineTri();
		void determineLength();
		void ecritFichier();
		void run();
};

