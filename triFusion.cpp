#include <iostream>
#include "declarationclasses.h"


using namespace std;

triFusion::triFusion(int *t,int length) {
	
	this->t=t;
	this->length=length;	
	this->nbr=0;
}
void triFusion::tri() {
	int *tab=tri_fusion(t,length);
	t=tab;
	ecritFichier();
	*fichier << "iteration -> " << nbr << endl;
}
std::string triFusion::getType() {
	return "tri fusion";	
}
int *triFusion::tri_fusion(int *t,int t_length) {
	if (t_length<=1)
		return t;
	int q=t_length/2;
	
	int *t1=new int[q];
	for (int i=0;i<q;i++) 
		t1[i]=t[i];
	int *t2=new int[t_length-q];	
	for (int i=q;i<t_length;i++) 
		t2[i-q]=t[i];
	t1=tri_fusion(t1,q);
	t2=tri_fusion(t2,t_length-q);
	ecritFichier(t1,q);
	*fichier << " + " ;
	ecritFichier(t2,t_length-q);
	*fichier << " -> ";
	int *tab=Fusion(t1,q,t2,t_length-q);
	ecritFichier(tab,t_length);
	*fichier << endl;
	nbr++;
	delete t1;
	delete t2;
	if (t_length<length) {
		delete t;		
	}
	return tab;
}
int *triFusion::Fusion(int *t1,int t1_length,int *t2,int t2_length) {
	int *t=new int [t1_length+t2_length];
	int i=0,j=0,k=0;
	for (k=0;k<(t1_length+t2_length);k++) {
		if (t1[i]<t2[j]) {
			t[k]=t1[i++];
			if (i==(t1_length+1)) {
				break;
			}
		} else { 
			t[k]=t2[j++];
			if (j==(t2_length+1)) {
				break;
			}
		}
	}
	for (;k<(t1_length+t2_length);k++) 
		if (i==(t1_length+1)) {
			t[k]=t2[j++];
		} else if (j==(t2_length+1)) {
			t[k]=t1[i++];	
		}
	
	return t;
}
